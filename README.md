# Climate Risk at Oliver Wyman IAM Tools

This is an open-source tools to facilitate running IAM Climate Risk models (e.g. GCAM). 

## Features
* Auto install packages / modules / dependencies, including Java, R, GCAM, etc. 
* Allow scenario-based variable change in a DataFrame-like interface
* Allow automation for model running

## Prerequisites
* Python 3.10

## Install instructions 
1. Git clone this repository
2. Run `src/china_scenario/gcam_test_app.py` for GCAM