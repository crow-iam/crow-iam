import os
import json
from shutil import copyfile
from ctypes import byref, WinDLL, sizeof, wintypes
import atexit

class Lock:
    registered = False
    paths = set()
    def __init__(self, dir_path):
        self.data = []
        self.dir_path = dir_path
        self.lock_path = os.path.join(dir_path, "mutex.lock")

    def occupied(self):
        return os.path.isfile(self.lock_path) and Lock.__process_running(self.__get_pid())
    @staticmethod
    def __process_running(pid):
        psapi = WinDLL('Psapi.dll')
        process_ids = (wintypes.DWORD*1024)()
        cb = sizeof(process_ids)
        bytes_returned = wintypes.DWORD()
        psapi.EnumProcesses(byref(process_ids), cb, byref(bytes_returned))
        return pid in process_ids

    def __get_pid(self):
        with open(self.lock_path) as r:
            return int(r.read())
    
    def __register(self):
        Lock.paths.add(self.lock_path)
        if not Lock.registered:
            atexit.register(Lock.exit_handler)
        VersionController.registered = True

    def acquire(self):
        self.__register()   
        while self.occupied():
            pass     
        os.makedirs(self.dir_path, exist_ok=True)
        with open(self.lock_path, 'w') as w:
            w.write(str(os.getpid()))
    
    @staticmethod
    def exit_handler():
        for path in Lock.paths:
            if os.path.isfile(path):
                os.remove(path)
    
    def on(self):
        with open(self.lock_path) as r:
            return int(r.read()) == os.getpid()

    def assure(self):
        if not self.on():
            self.acquire()
    
    def release(self):
        if os.path.isfile(self.lock_path):
            os.remove(self.lock_path)
    
    def __enter__(self):
        self.acquire()
    
    def __exit__(self, exception_type, exception_value, traceback):
        self.release()

class VersionController:
    def __init__(self, dir_path):
        self.data = []
        self.dir_path = dir_path
        self.data_path = os.path.join(dir_path, "version.json")
        self.lock = Lock(dir_path)
        if not os.path.isfile(self.data_path):
            self.save()
        else:
            self.load()

    def save(self):
        with open(self.data_path, "w") as w:
            json.dump(self.data,w)

    def load(self):
        with open(self.data_path, 'r') as r:
            self.data = json.load(r)

        
    def record(self, **kwargs):
        self.data.append(kwargs)
        self.save()
   
    def __copyfile(self, backup, main):
        self.lock.assure()
        print("Copying from", backup, "to", main)
        copyfile(backup, main)
    def __remove(self, path):
        self.lock.assure()
        print("Removing", path)
        os.remove(path)
    def __write(self, path, value):
        self.lock.assure()
        with open(path, 'wb') as wb:
            wb.write(value)

  

    def modify(self, path, value):
        # Back up
        backup_path = path + ".backup"
        if not os.path.isfile(backup_path):
            self.__copyfile(path, backup_path)
            self.record(
                action="update",
                main=path,
                backup=backup_path
            )
        else:
            print("Assuring backup for", path)
        print("Modifying", path)
        self.__write(path, value)

    def revert(self):
        for commit in self.data[::-1]:
            revert_copy_path = commit['main'] + ".revertcopy"
            self.__copyfile(commit['main'], revert_copy_path)
            self.__copyfile(commit['backup'], commit['main'])
            self.__remove(revert_copy_path)
            self.__remove(commit['backup'])
        self.data.clear()
        self.save()
