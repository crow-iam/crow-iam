from package_manager import Package
from types import SimpleNamespace
import json
import os
from crow import GcamModel
from version_controller import VersionController
import subprocess

Package.assure_installed("gcam", "r", "jdk")

class GcamSession:
    def __init__(self, continue_last_session=False, gcam_config=None):
        gcam_config = gcam_config or Package.get("gcam").gcam_config
        self.continue_last_session = continue_last_session
        with open(gcam_config) as r:
            self.config = SimpleNamespace(**json.load(r))
        self.gcam = GcamModel(
            [
                Package.get("gcam").input,
                Package.get("gcam").policy,
                Package.get("gcam").code
            ],
            dependency=self.config.dependency)
        self.version_controller = VersionController(Package.get("gcam").root)
    def __enter__(self):
        self.version_controller.lock.__enter__()
        if not self.continue_last_session:
            self.version_controller.revert()
        else:
            raise UserWarning("You chose to continue last session, therefore the file system is not reverted. ")
        return self

    def __exit__(self, *args):
        self.version_controller.lock.__exit__(*args)
    
    def rebuild_input(self):
        for source in self.gcam.sources:
            if source.modified():
                self.version_controller.modify(source.path, source.to_bytes())
        environment = os.environ.copy()
        result = subprocess.run(["pkg\\jdk\\jdk-17.0.2\\bin\\java.exe", "XMLDBDriver", "--print-java-home"], capture_output=True, text=True, env=environment)
        environment["JAVA_HOME"] = result.stdout.strip()
        java_bin = os.path.join(environment["JAVA_HOME"], "bin")
        java_server = os.path.join(environment["JAVA_HOME"], "bin", "server")
        environment["PATH"] = ";".join((java_bin, java_server))
        process = subprocess.run([
            Package.get("r").rscript,
            "modules\\crow\\gcam.R"
        ], env=environment)
        if process.returncode>0:
            raise UserWarning("Warning: Problems arose from GCAMData. ")


    def run(self):
        self.gcam.run(
            gcam_cwd=Package.get("gcam").dir,
            gcam = Package.get("gcam").exe,
            config = Package.get("gcam").configuration,
            lib = Package.get("gcam").jars,
            xmldb = Package.get("gcam").xmldb,
            java = Package.get("jdk").java,
            logconf = Package.get("gcam").logconf
        )
    
        
    
    @property
    def tables(self):
        return self.gcam.tables
    
    @property
    def sources(self):
        return self.gcam.sources
    