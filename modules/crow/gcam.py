import os
from itertools import takewhile, product
import pandas as pd
from .base import BaseModel
from typing import Optional
from io import BytesIO
from hashlib import sha256
import subprocess
from types import SimpleNamespace
import xml.etree.ElementTree as ElementTree

class Source:
    @staticmethod
    def from_path(path):
        filename = os.path.split(path)[-1]
        name, extension = os.path.splitext(filename)
        if extension in {'.csv'}: constructor = CsvSource
        elif extension in {'.R'}: constructor = TextSource
        elif extension in {'.xml'}: constructor = XmlSource
        return constructor(name, path)

    def __init__(self, name, path, data=None):
        self.__name = name
        self.__path = path
        self.__data = data
        _, self.__extension = os.path.splitext(path)
        self.__initial_hash = self.hash() if data else None
    

    @property
    def name(self):
        return self.__name

    @property
    def path(self):
        return self.__path
    
    @property
    def path_rname(self):
        ret = self.path
        # Only preserve the last two parts
        ret = os.path.sep.join(ret.split(os.path.sep)[-2:])
        # Replace the backlash to frontlash
        ret = ret.replace(os.path.sep, '/')
        # Remove the extension
        ret, _ = os.path.splitext(ret)
        return ret


    @property
    def data(self):
        if self.__path and self.__data is None:
            self.__data = self.load()
            self.__initial_hash = self.hash()
        return self.__data
    
    @property
    def extension(self):
        return self.__extension
    
    @data.setter
    def data(self, value):
        self.__data = value
        
    def __repr__(self):
        return f"<Source {self.name}{self.__extension}>"
    
    def load(self, *args, **kwargs):
        return ...

    def hash(self):
        return ...
    
    def modified(self):
        if self.__initial_hash is None:
            return self.__data is not None
        else:
            return self.__initial_hash != self.hash()

class CsvSource(Source):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.comment = ""
    def __load_comment(self):
        with open(self.path, encoding="utf-8") as r:
            self.comment = (''.join(takewhile(lambda line:line.startswith('#'), r.readlines())))
    def load(self, *args, **kwargs):
        self.__load_comment()
        return pd.read_csv(self.path, *args, comment='#', on_bad_lines="warn", **kwargs)
    def hash(self):
        return sha256(pd.util.hash_pandas_object(self.data, index=True).values).hexdigest()
    def to_bytes(self):
        self.__load_comment()
        buffer = BytesIO()
        buffer.write(self.comment.encode('utf-8'))
        self.data.to_csv(buffer, index=False)
        return buffer.getvalue()


class TextSource(Source):
    def load(self, *args, **kwargs):
        with open(self.path, *args, encoding='utf-8', **kwargs) as r:
            return r.read()
    def hash(self):
        return sha256(self.data.encode('utf-8')).hexdigest()
    def to_bytes(self):
        return self.data.encode("utf-8")

class XmlSource(Source):
    def load(self, *args, **kwargs):
        return ElementTree.parse(self.path, *args, **kwargs)
    def hash(self):
        return sha256(ElementTree.tostring(self.data.getroot(), encoding='utf-8')).hexdigest()
    def to_bytes(self):
        return ElementTree.tostring(self.data.getroot(), encoding='utf-8')


class Sources:
    def __init__(self, sources, dependency=None):
        self.sources = {source.name:source for source in sources}
        if not dependency:
            self.create_dependency()
        else:
            self.dependency = dependency
    
    def __repr__(self):
        return f"<Sources with {len(self.sources)} elements>"
    
    def __iter__(self) -> Source:
        yield from self.sources.values()
    
    @staticmethod
    def from_paths(*paths):
        return Sources((Source.from_path(path) for path in paths))
    
    @staticmethod
    def from_walk(paths, extensions, dependency=None):
        final_paths = []
        for path in paths:
            for dirpath, _, filenames in os.walk(path):
                for filename in filenames:
                    _, extension = os.path.splitext(filename)
                    if extension in extensions:
                        final_paths.append(Source.from_path(os.path.join(dirpath, filename)))
        return Sources(final_paths, dependency)

    
    def __setitem__(self, key, df:pd.DataFrame):
        if key in self.sources:
            self.sources[key].data = df
        else:
            self.sources[key] = Source(
                name=df.name,
                path="",
                data=df
            )
       
    def create_dependency(self):
        self.dependency = {}
        r_sources = [source for source in self.sources.values() if source.extension == '.R']
        csv_sources = [source for source in self.sources.values() if source.extension in '.csv']
        for r_source, csv_source in product(r_sources, csv_sources):
            if csv_source.path_rname in r_source.data:
                self.dependency.setdefault(csv_source, []).append(r_source)
    
    def __getitem__(self, key):
        return self.sources[key].data
    
    def filter(self, extension):
        return Sources(
            sources=[source for source in self.sources.values() if source.extension == extension],
            dependency=self.dependency
        )
    

class GcamModel (BaseModel):
    def __init__(self, paths, dependency):
        self.sources = Sources.from_walk(paths, [".csv", ".xml", ".R"])
        self.tables = self.sources.filter(extension=".csv")
    
    @property
    def variables(self):
        for table in self.tables:
            yield from table.iter_variables()
    

    def run(self, gcam_cwd, **_paths):
        for path_key in ["gcam", "config", "lib", "xmldb", "java", "logconf"]:
            if path_key not in _paths:
                raise TypeError(f"Missing keyword argument to specify the path for {path_key}")
        paths = SimpleNamespace(**{key:os.path.join(os.getcwd(),value) for key,value in _paths.items()})
        environment = os.environ.copy()
        environment["CLASSPATH"] = ";".join([paths.lib, paths.xmldb])
        result = subprocess.run([paths.java, "XMLDBDriver", "--print-java-home"], capture_output=True, text=True, env=environment)
        environment["JAVA_HOME"] = result.stdout.strip()
        java_bin = os.path.join(environment["JAVA_HOME"], "bin")
        java_server = os.path.join(environment["JAVA_HOME"], "bin", "server")
        environment["PATH"] = ";".join((java_bin, java_server))
        subprocess.run([paths.gcam, "-C", paths.config, "-L", paths.logconf], env=environment, cwd=gcam_cwd)
    