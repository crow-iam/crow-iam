import json
import os
from requests import get
from requests.exceptions import ConnectionError
from types import SimpleNamespace
from queue import Queue
import subprocess
from tqdm import tqdm

class Package:
    catalog = {}
    objects = {}

    def __init__(self, config):
        assert 'name' in config
        self.config = config
        self.name = config['name']
        self.installer = config['install']
        self.__dict__.update(**config.get('paths',{}))
        Package.objects.setdefault(self.name, self)
    
    def check_installed(self, name=None):
        check_name = name or self.name
        return os.path.isdir(f"pkg/{check_name}")
    
    def __repr__(self):
        return f"<Package {self.name}>"

    
    def __handler_unarchive(self, mime_path, home_path, **config):
        subprocess.run([
            config.get("path", "7z.exe"),
            "x", mime_path,
            "-o"+home_path,
            "-r", "-y"
        ], stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    
    def install(self, force=False, proxy=None,handlers_config=None):
        _handlers_config = handlers_config or {}
        os.makedirs("pkg", exist_ok=True)
        os.makedirs("tmp", exist_ok=True)
        HANDLERS = {
            'unarchive': self.__handler_unarchive
        }
        status = SimpleNamespace(
            name=self.name,
            temp_path=os.path.join('tmp',f"{self.name}-install.pkg"),
            home_path=f"pkg/{self.name}",
            url=self.installer.get('url'),
            installed=self.check_installed(),
            force=force,
            dependencies=self.config.get('dependency', [])
        )
        if status.installed and not force:
            return status
        print("Installing", self.name)
        status.temp_cached = os.path.isfile(status.temp_path)
        for dependency in status.dependencies:
            if not self.check_installed(dependency):
                raise FileNotFoundError(dependency)
        # Download
        if status.url and not status.temp_cached:
            with get(status.url, stream=True, proxies=proxy) as r:
                total_bytes = int(r.headers.get('content-length',0))
                progress_bar = tqdm(total=total_bytes, unit='iB', unit_scale=True)
                r.raise_for_status()
                with open(status.temp_path, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        progress_bar.update(len(chunk))
                        f.write(chunk)
        for handler_name in self.installer.get("handlers", []):
            handler = HANDLERS.get(handler_name, lambda *_:_)
            handler(status.temp_path, status.home_path, **_handlers_config.get(handler_name,{}))
        return status
    
    @staticmethod
    def assure_installed(*names):
        with open("config/package_config.json") as r:
            Package.catalog = json.load(r)
        queue = Queue()
        config_dictionary = {config['name']:config for config in Package.catalog['packages'] if config['name'] in names}
        for config in config_dictionary.values():
            queue.put(Package(config))
        while not queue.empty():
            item = queue.get()
            try:
                item.install(proxy=Package.catalog.get('proxy',{}), handlers_config=Package.catalog.get('handlers',{}))
            except FileNotFoundError as e:
                print("Dependency file not found for", str(e), "retrying")
                queue.put(item)
            except ConnectionError as e:
                print("Connection error for", str(e), "retrying")
                queue.put(item)
        
    @staticmethod
    def get(name):
        return Package.objects[name]