from package_manager import Package
from crow import GcamSession

Package.assure_installed("gcam", "r", "jdk")

with GcamSession() as gcam:
    # gcam.rebuild_input()
    gcam.run() # Java-based script XML -> Output